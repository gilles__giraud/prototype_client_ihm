/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.ReparationForm;
import Controller.ControllerReparatorMenuWindow;
import Model.User;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author charles.santerre
 */
public class ReparatorMenuWindow extends JFrame {

    User userLogged;
    /**
     * Principal panel
     */
    JPanel panel = new JPanel();

    JButton openListVehicle = new JButton("open list");
    
    JButton openReparationForm = new JButton("open reparation form (proto)");

    JButton disconnectButton = new JButton("disconnect");

    ControllerReparatorMenuWindow controller = new ControllerReparatorMenuWindow();

    public ReparatorMenuWindow(User userLogged) {
        this.userLogged = userLogged;
        BoxLayout panelLayout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(panelLayout);

        /*/**
         * Create Menu Bar
        
        JMenuBar menu= new JMenuBar();
        setJMenuBar(menu);
        JMenu tools = new JMenu("Tools");
        menu.add(tools);
        JMenu help = new JMenu("Help");
        menu.add(help);
        
        /**
         * Implement Menu Item for tools Menu
         
        JMenuItem toolsItem1 = new JMenuItem("Options");
        tools.add(toolsItem1);
        JMenuItem toolsItem2 = new JMenuItem("About");
        help.add(toolsItem2);*/
        /**
         * Create panels
         */
        JPanel titlePanel = new JPanel(new FlowLayout());
        JPanel listPanel = new JPanel(new FlowLayout());
        JPanel buttonPanel = new JPanel(new FlowLayout());

        /**
         * Add JLabel name of prototype
         */
        
        titlePanel.add(new JLabel(userLogged.getFirstname() + " " + userLogged.getLastname() + " - " + userLogged.getTypeuser()));
        titlePanel.add(new JLabel("Menu"));

        /**
         * Buttons Listener
         */
        openReparationForm.addMouseListener(new openReparationFormClick());
        
        openListVehicle.addMouseListener(new openListVehicleWaitingReparationClick());

        disconnectButton.addMouseListener(new disconnectClick());

        /**
         * Add buttons
         */
        buttonPanel.add(openReparationForm);
        buttonPanel.add(openListVehicle);
        buttonPanel.add(disconnectButton);

        /**
         * Add panels to the window
         */
        panel.add(titlePanel);
        panel.add(buttonPanel);
        this.add(panel);

        /**
         * Parameters of the window
         */
        this.setSize(350, 250);
        this.setTitle("Client Prototype");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }
    
    class openReparationFormClick extends MouseAdapter{
        public void mouseClicked(MouseEvent args){
            try{
                ReparationForm answerRepForm = controller.choiceGiveReparationForm();
                if(answerRepForm != null){
                    ReparatorMenuWindow.this.dispose();
                }else{
                    JOptionPane.showMessageDialog(panel, controller.getErrorMessage(), "Search error", JOptionPane.ERROR_MESSAGE);
                }
            }catch (UnknownHostException e) {
                JOptionPane.showMessageDialog(panel, "Server is unreachable (Unknown Host)", "Connection error", JOptionPane.ERROR_MESSAGE);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(panel, e.getMessage(), "Connection error", JOptionPane.ERROR_MESSAGE);
            }catch(ParseException e){
                JOptionPane.showMessageDialog(panel, e.getMessage(), "Parsing Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    class disconnectClick extends MouseAdapter{
        public void mouseClicked(MouseEvent args){
            ReparatorMenuWindow.this.dispose();
            new ConnectWindow();
        }
    }
    
    class openListVehicleWaitingReparationClick extends MouseAdapter{
        public void mouseClicked(MouseEvent args){
            try {
                boolean answer = controller.choiceListVehicleWaiting();
                if (answer) {
                    ReparatorMenuWindow.this.dispose();
                }else{
                    JOptionPane.showMessageDialog(panel, controller.getErrorMessage(), "Search error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (UnknownHostException e) {
                JOptionPane.showMessageDialog(panel, "Server is unreachable (Unknown Host)", "Connection error", JOptionPane.ERROR_MESSAGE);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(panel, e.getMessage(), "Connection error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
