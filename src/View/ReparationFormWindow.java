/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.ReparationForm;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author pds_user
 */
public class ReparationFormWindow extends JFrame{
    
    /**
     * Principal panel
     */
    JPanel panel = new JPanel();
    
    JButton openListVehicle = new JButton("open list");
    
    ReparationForm repForm;
    
    public ReparationFormWindow(ReparationForm repForm){
        this.repForm = repForm;
        
        /**
         * Parameters of the window
         */
        this.setSize(350,250);
        this.setTitle("Client Prototype");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        
        this.add(new JLabel(repForm.toString()));
    }
    
}
