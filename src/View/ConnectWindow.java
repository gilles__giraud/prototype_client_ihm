package View;

import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import Controller.ControllerConnectWindow;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * Main window
 */
public class ConnectWindow extends JFrame {

    /**
     * Principal panel
     */
    JPanel panel = new JPanel();

    JButton cancelButton = new JButton("Cancel");

    JButton okButton = new JButton("OK");

    JButton viewPwdButton = new JButton("View");

    JTextField loginField = new JTextField(10);

    JPasswordField passwordField = new JPasswordField(10);
    char defaultPwd;

    ControllerConnectWindow controller = new ControllerConnectWindow();

    /**
     * Create window
     */
    public ConnectWindow() {
        BoxLayout panelLayout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(panelLayout);

        /**
         * Create Menu Bar
         */
        JMenuBar menu = new JMenuBar();
        setJMenuBar(menu);
        JMenu tools = new JMenu("Tools");
        menu.add(tools);
        JMenu help = new JMenu("Help");
        menu.add(help);

        /**
         * Implement Menu Item for tools Menu
         */
        JMenuItem toolsItem1 = new JMenuItem("Options");
        tools.add(toolsItem1);
        JMenuItem toolsItem2 = new JMenuItem("About");
        help.add(toolsItem2);

        /**
         * Create panels
         */
        JPanel titlePanel = new JPanel(new FlowLayout());
        JPanel loginPanel = new JPanel(new FlowLayout());
        JPanel passwordPanel = new JPanel(new FlowLayout());
        JPanel listPanel = new JPanel(new FlowLayout());
        JPanel buttonPanel = new JPanel(new FlowLayout());

        /**
         * Add JLabel name of prototype
         */
        titlePanel.add(new JLabel("Connect to access the application"));

        /**
         * Add JLabel and JTextField for Login
         */
        loginPanel.add(new JLabel("Login :"));
        loginPanel.add(loginField);

        /**
         * Add JLabel and JTextField for Password
         */
        passwordPanel.add(new JLabel("Password :"));
        passwordPanel.add(passwordField);
        passwordPanel.add(viewPwdButton);

        
        /**
         * Field Listener
         */
        
        passwordField.addKeyListener(new FieldAdapter());
        loginField.addKeyListener(new FieldAdapter());
        
        /**
         * Buttons Listener
         */
        okButton.addMouseListener(new connectionButtonClick());

        cancelButton.addMouseListener(new cancelButtonClick());

        viewPwdButton.addMouseListener(new showButtonClick());

        /**
         * Add buttons
         */
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        /**
         * Add panels to the window
         */
        panel.add(titlePanel);
        panel.add(loginPanel);
        panel.add(passwordPanel);
        panel.add(buttonPanel);
        this.add(panel);

        /**
         * Parameters of the window
         */
        this.setSize(350, 250);
        this.setTitle("Client Prototype");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }
    
    public void connectionMethod(){
        try {
            String answer = controller.openMenuWindow(loginField.getText(), passwordField.getText());
            if (answer.equals("success")) {
                ConnectWindow.this.dispose();
            } else if (answer.equals("empty")) {
                JOptionPane.showMessageDialog(panel, "Please enter a login and a password", "Connection error", JOptionPane.ERROR_MESSAGE);
            } else if (answer.equals("wrong")) {
                JOptionPane.showMessageDialog(panel, controller.getErrorMessage(), "Connection error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (UnknownHostException e) {
            JOptionPane.showMessageDialog(panel, "Server is unreachable (Unknown Host)", "Connection error", JOptionPane.ERROR_MESSAGE);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(panel, e.getMessage(), "Connection error", JOptionPane.ERROR_MESSAGE);
        } catch (ParseException e){
            JOptionPane.showMessageDialog(panel, e.getMessage(), "Connection error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    class FieldAdapter extends KeyAdapter{
        @Override
        public void keyTyped(KeyEvent args){
            if(args.getKeyChar() == KeyEvent.VK_ENTER)
                connectionMethod();
        }
    }

    class connectionButtonClick extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent args) {
            connectionMethod();
        }
    }

    class cancelButtonClick extends MouseAdapter {
        @Override    
        public void mouseClicked(MouseEvent args) {
            loginField.setText("");
            passwordField.setText("");
        }
    }

    class showButtonClick extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            defaultPwd = passwordField.getEchoChar();
            passwordField.setEchoChar((char) 0);
        }
        @Override
        public void mouseReleased(MouseEvent e) {
            passwordField.setEchoChar(defaultPwd);
        }
    }
}
