/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author charles.santerre
 */
public class Part {
    
    /**
     *
     * Declaration of attribute
     * 
     * @param id
     * @param stock
     * @param description
     * @param purchasePrice
     */
    private int id;
    private int stock;
    private String description;
    private float purchasePrice;
    
    

    public int getId() {
        return id;
    }

    public int getStock() {
        return stock;
    }

    public String getDescription() {
        return description;
    }

    public float getPurchasePrice() {
        return purchasePrice;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPurchasePrice(float purchasePrice) {
        this.purchasePrice = purchasePrice;
    }
}
