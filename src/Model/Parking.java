/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author pds_user
 */
public class Parking {
    
    /**
     * @param id
     * @param occupied
     */
    private int id;
    private boolean occupied;

    public Parking(int id, boolean occupied) {
        this.id = id;
        this.occupied = occupied;
    }

    public Parking(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }
    
    
    
    public static String serialize(Parking parking){
        if(parking != null){
            return parking.id + "///" + parking.occupied;
        }else{
            return "null";
        }
    }
    
    public static Parking deserialize(String parking){
        if(parking.equals("null")){
            return null;
        }else{
            String[] eachAttributes = parking.split("///");
            return new Parking(Integer.parseInt(eachAttributes[0]), Boolean.parseBoolean(eachAttributes[1]));
        }
    }
    
}
