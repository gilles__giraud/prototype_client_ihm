/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author charles.santerre
 */
public class Car {
    
    /**
     * declaration of attribute
     * @param id
     * @param matriculation
     * @param purchaseDate
     * @param lastRevision
     */
    private int id;
    private String matriculation;
    private String purchaseDate; //change into date later
    private String lastRevision; //change into date later
    
    public Car(int anId, String aMatriculation, String aPurchaseDate, String aLastRevision){
        id = anId;
        matriculation = aMatriculation;
        purchaseDate = aPurchaseDate;
        lastRevision = aLastRevision;
    }

    public int getId() {
        return id;
    }

    public String getMatriculation() {
        return matriculation;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public String getLastRevision() {
        return lastRevision;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMatriculation(String matriculation) {
        this.matriculation = matriculation;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public void setLastRevision(String lastRevision) {
        this.lastRevision = lastRevision;
    }
    
    
    
    public static String serialize(Car car){
        if(car != null){
            return car.id + "///" + car.matriculation + "///" + car.purchaseDate + "///" + car.lastRevision;
        }else{
            return "null";
        }
    }
    
    public static Car deserialize(String car){
        if(car.equals("null")){
            return null;
        }else{
            String[] eachAttributes = car.split("///");
            return new Car(Integer.parseInt(eachAttributes[0]), eachAttributes[1], eachAttributes[2], eachAttributes[3]);
        }
    }
}
