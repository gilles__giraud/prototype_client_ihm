/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author VM7SS
 */
public class User {
    
    private int id;
    private String typeuser;
    private String firstname;
    private String lastname;
    private String address;
    private String town;
    private String postalcode;
    private String login;
    private String email;
    private Date hiringDate;
    private float incomingPerHour;

    public User(int id, String typeuser, String firstname, String lastname, String address, String town, String postalcode, String login, String email, Date hiringDate, float incomingPerHour) {
        this.id = id;
        this.typeuser = typeuser;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.town = town;
        this.postalcode = postalcode;
        this.login = login;
        this.email = email;
        this.hiringDate = hiringDate;
        this.incomingPerHour = incomingPerHour;
    }

    @Override
    public String toString() {
        return "user{" + "id=" + id + ", typeuser=" + typeuser + ", firstname=" + firstname + ", lastname=" + lastname + ", address=" + address + ", town=" + town + ", postalcode=" + postalcode + ", login=" + login + ", email=" + email + ", hiringDate=" + hiringDate + ", incomingPerHour=" + incomingPerHour + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeuser() {
        return typeuser;
    }

    public void setTypeuser(String typeuser) {
        this.typeuser = typeuser;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getHiringDate() {
        return hiringDate;
    }

    public void setHiringDate(Date hiringDate) {
        this.hiringDate = hiringDate;
    }

    public float getIncomingPerHour() {
        return incomingPerHour;
    }

    public void setIncomingPerHour(float incomingPerHour) {
        this.incomingPerHour = incomingPerHour;
    }
    
    public static String serialize(User user){
        return user.id + "///" + user.id + "///" + user.typeuser + "///" + user.firstname + "///" + user.lastname + "///" + user.address + "///" + user.town+ "///" + user.postalcode + "///" + user.login + "///" + user.email + "///"  + user.hiringDate.toString() + "///"  + user.incomingPerHour;
    }
    
    public static User deserialize(String user) throws ParseException{
        String[] eachAttributes = user.split("///");
        DateFormat df = new SimpleDateFormat("YYYY-MM-DD");
        return new User(Integer.parseInt(eachAttributes[0]), eachAttributes[1], eachAttributes[2], eachAttributes[3], eachAttributes[4], eachAttributes[5], eachAttributes[6], eachAttributes[7], eachAttributes[8], df.parse(eachAttributes[9]), Float.parseFloat(eachAttributes[10]));
    }
}
