/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pds_user
 */
public class ReparationForm {
    /**
     *
     * Declaration of attribute
     * 
     * @param id
     * @param stock
     * @param description
     * @param purchasePrice
     */
    private int id;
    private String descriptionCardState;
    private String descriptionUrgencyDegree;
    private Parking parkingPlace;
    private Car car;
    private Bike bike;
    private Date  entryDate;
    private Date outDate;
    private String diagnosis;
    private String reparationDetail;
    
    private ArrayList<Part> listPartUsed = new ArrayList<>();

    public ReparationForm(int id, String descriptionCardState, String descriptionUrgencyDegree, Parking parkingPlace, Car car, Bike bike, Date entryDate, Date outDate, String diagnosis, String reparationDetail) {
        this.id = id;
        this.descriptionCardState = descriptionCardState;
        this.descriptionUrgencyDegree = descriptionUrgencyDegree;
        this.parkingPlace = parkingPlace;
        this.car = car;
        this.bike = bike;
        this.entryDate = entryDate;
        this.outDate = outDate;
        this.diagnosis = diagnosis;
        this.reparationDetail = reparationDetail;
    }

    public int getId() {
        return id;
    }

    public String getDescriptionCardState() {
        return descriptionCardState;
    }

    public String getDescriptionUrgencyDegree() {
        return descriptionUrgencyDegree;
    }

    public Parking getParkingPlace() {
        return parkingPlace;
    }

    public Car getCar() {
        return car;
    }

    public Bike getBike() {
        return bike;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public Date getOutDate() {
        return outDate;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getReparationDetail() {
        return reparationDetail;
    }

    public ArrayList<Part> getListPartUsed() {
        return listPartUsed;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescriptionCardState(String descriptionCardState) {
        this.descriptionCardState = descriptionCardState;
    }

    public void setDescriptionUrgencyDegree(String descriptionUrgencyDegree) {
        this.descriptionUrgencyDegree = descriptionUrgencyDegree;
    }

    public void setParkingPlace(Parking parkingPlace) {
        this.parkingPlace = parkingPlace;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public void setOutDate(Date outDate) {
        this.outDate = outDate;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public void setReparationDetail(String reparationDetail) {
        this.reparationDetail = reparationDetail;
    }

    public void setListPartUsed(ArrayList<Part> listPartUsed) {
        this.listPartUsed = listPartUsed;
    }

    @Override
    public String toString() {
        return "ReparationForm{" + "id=" + id + ", descriptionCardState=" + descriptionCardState + ", descriptionUrgencyDegree=" + descriptionUrgencyDegree + ", parkingPlace=" + parkingPlace + ", car=" + car + ", bike=" + bike + ", entryDate=" + entryDate + ", outDate=" + outDate + ", diagnosis=" + diagnosis + ", reparationDetail=" + reparationDetail + ", listPartUsed=" + listPartUsed + '}';
    }
    
    
    public static Map<String, String> serializeAll(ReparationForm repForm){
        Map<String, String> result = new HashMap<String, String>();
        result.put("ReparationForm", repForm.id + "///" + repForm.descriptionCardState + "///" + repForm.descriptionUrgencyDegree + "///" + repForm.entryDate.toString() + "///" + repForm.outDate + "///" + repForm.diagnosis + "///" + repForm.reparationDetail);
        result.put("Bike", Bike.serialize(repForm.bike));
        result.put("Car", Car.serialize(repForm.car));
        result.put("Parking", Parking.serialize(repForm.parkingPlace));
        return result;
    }
    
    public static ReparationForm deserializeAll(String repString, String carString, String bikeString, String parkingString) throws ParseException{
        Car car = Car.deserialize(carString);
        Bike bike = Bike.deserialize(bikeString);
        Parking parking = Parking.deserialize(parkingString);
        String[] eachAttributes = repString.split("///");
        DateFormat df = new SimpleDateFormat("YYYY-MM-DD");
        return new ReparationForm(Integer.parseInt(eachAttributes[0]), eachAttributes[1], eachAttributes[2], parking, car, bike, df.parse(eachAttributes[3]), df.parse(eachAttributes[4]), eachAttributes[5], eachAttributes[6]);
    }
}
