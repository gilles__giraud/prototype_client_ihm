/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import CommunicationModel.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author VM7SS
 */
public class VehicleDAO {
    
    private static JsonObject getListVehicleRequestJson(){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "giveListVehicleReparation");
        builder.add("ConnectionState", "Try");
        json = builder.build();
        return json;
    }
    
    public static JsonObject getListVehicleWaitingForReparation() throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getListVehicleRequestJson());
        result = com.getData();
        com.close();
        return result;
    }
    
}
