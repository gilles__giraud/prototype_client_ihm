/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author pds_user
 */
public class Bike {
    
    private int id;
    private String purchaseDate; //change into date later
    private String lastRevision; //change into date later

    public int getId() {
        return id;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public String getLastRevision() {
        return lastRevision;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public void setLastRevision(String lastRevision) {
        this.lastRevision = lastRevision;
    }

    public Bike(int id, String purchaseDate, String lastRevision) {
        this.id = id;
        this.purchaseDate = purchaseDate;
        this.lastRevision = lastRevision;
    }
    
    public static String serialize(Bike bike){
        if(bike != null){
            return bike.id + "///" + bike.purchaseDate + "///" + bike.lastRevision;
        }else{
            return "null";
        }
    }
    
    public static Bike deserialize(String bike){
        if(bike.equals("null")){
            return null;
        }else{
            String[] eachAttributes = bike.split("///");
            return new Bike(Integer.parseInt(eachAttributes[0]), eachAttributes[1], eachAttributes[2]);
        }
    }
    
}
