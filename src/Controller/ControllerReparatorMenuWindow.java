/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.ReparationForm;
import Model.ReparationFormDAO;
import Model.VehicleDAO;
import View.ReparationFormWindow;
import java.io.IOException;
import java.text.ParseException;
import javax.json.JsonObject;

/**
 *
 * @author pds_user
 */
public class ControllerReparatorMenuWindow {
    
    String errorMessage = "";

    public ControllerReparatorMenuWindow() {

    }

    public boolean choiceListVehicleWaiting() throws IOException {
        boolean bool = false;
        JsonObject inputJson = VehicleDAO.getListVehicleWaitingForReparation();
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                bool = true;
                
                break;
            case "Error":
                break;
        }
        return bool;
    }
    
    public ReparationForm choiceGiveReparationForm() throws IOException, ParseException{
        ReparationForm repForm = null;
        JsonObject inputJson = ReparationFormDAO.getReparationForm(1);
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                repForm = ReparationForm.deserializeAll(inputJson.getString("ReparationForm"), inputJson.getString("Car"), inputJson.getString("Bike"), inputJson.getString("Parking"));
                new ReparationFormWindow(repForm);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return repForm;
    }
    
    public String getErrorMessage(){
        return this.errorMessage;
    }
}
