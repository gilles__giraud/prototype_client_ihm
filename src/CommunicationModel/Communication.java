/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CommunicationModel;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParser;
import static View.main.Config;

/**
 *
 * @author VM7SS
 */
public class Communication {
    
    private Socket s;
    private PrintWriter pw;
    private DataInputStream in;
    
    public Communication() throws UnknownHostException, IOException{
        this.initializeSocket();
    }
    
    private void initializeSocket() throws UnknownHostException, IOException{
        s = new Socket(Config.SERVER_IP, Config.SERVER_PORT);
        pw = new PrintWriter(new DataOutputStream(s.getOutputStream()), true);
        in = new DataInputStream(s.getInputStream());
    }
    
    public void sendData(JsonObject json) throws IOException{
        pw.println(json.toString());
    }
    
    public JsonObject getData() throws IOException{
        JsonReader reader = Json.createReader(in);
        return reader.readObject();
    }
    
    public void close() throws IOException{
        pw.println("END");
        in.close();
        pw.close();
        s.close();
    }
}
