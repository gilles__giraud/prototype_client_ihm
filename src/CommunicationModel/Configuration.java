package CommunicationModel;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * This class will be used to create a configuration object. This object will contain the server application IP and the host IP
 */
public class Configuration {
    
    public String SERVER_IP;
    public String HOST_IP;
    public int SERVER_PORT;
    
    public Configuration(){
        try{
            init();
        }catch(UnknownHostException e){
            System.exit(1);
        }
    }
    
    private void init() throws UnknownHostException{
        SERVER_IP = "192.168.0.16";
        InetAddress IP = InetAddress.getLocalHost();
        HOST_IP = IP.getHostAddress();
        SERVER_PORT = 10080;
    }
}
